This is a fixed version of jetbrains "Getting Started with Spring MVC, Hibernate and JSON" tutorial posted on:
https://confluence.jetbrains.com/display/IntelliJIDEA/Getting+Started+with+Spring+MVC,+Hibernate+and+JSON

since I saw lot's of questions and uncertainties I located it here for those who want to avoid fixing themselves and concentrate on the subject