package com.springapp.mvc;

/**
 * Created by vladimir on 7/10/15.
 */
import org.springframework.data.jpa.repository.JpaRepository;

public interface UserRepository extends JpaRepository<User, Long> {
}